<?php

namespace Axn\FormBuilder;

use Illuminate\Support\ServiceProvider;
use Axn\FormBuilder\Commands\FormBuilderCommand;

class FormBuilderServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected string $packageName = "axn-form-builder";

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                FormBuilderCommand::class
            ]);
        }
        // Views
        $this->loadViewsFrom(__DIR__."/../resources/views", $this->packageName);

        $this->publishes([
            __DIR__."/../resources/views" => resource_path("views/vendor/{$this->packageName}"),
        ], $this->packageName);

        // Config
        $this->publishes([
            __DIR__."/../config/{$this->packageName}.php" => config_path("{$this->packageName}.php"),
        ], $this->packageName);

        // Assets
        $this->publishes([
            __DIR__."/../public" => public_path("vendor/{$this->packageName}"),
        ], $this->packageName);
    }


    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__."/../config/{$this->packageName}.php", $this->packageName
        );
    }
}
