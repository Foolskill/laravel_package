<?php

namespace Axn\FormBuilder\Commands;

use Illuminate\Console\Command;

class FormBuilderCommand extends Command
{
    public $signature = 'axn-fb';

    public $description = 'This a the Axn-fb Artisan Command Line';

    public function handle()
    {
        $this->comment('All done');
    }
}
