<?php

namespace Axn\FormBuilder;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Axn\FormBuilder\FormBuilder
 */
class FormBuilderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'axn-laravel-form-builder';
    }
}
