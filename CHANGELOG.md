# Changelog

All notable changes to `axn-laravel-form-builder` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
